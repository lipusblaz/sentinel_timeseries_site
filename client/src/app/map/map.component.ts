import { Component, OnInit, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import OlMap from 'ol/Map';
import OlXYZ from 'ol/source/XYZ';
import OlTileLayer from 'ol/layer/Tile';
import OlView from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { fromLonLat } from 'ol/proj';
import GeoJSON from 'ol/format/GeoJSON';

// import readFileSync from "file-system";



@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {

  map: OlMap;
  source: OlXYZ;
  tlayer: OlTileLayer;
  vlayer1: VectorLayer;
  vlayer2: VectorLayer;
  view: OlView;
  http: HttpClient;

  // slovenija_proj = readFileSync("");

  ngOnInit() {
    this.source = new OlXYZ({
      url: 'http://tile.osm.org/{z}/{x}/{y}.png'
    });

    this.tlayer = new OlTileLayer({
      source: this.source
    });

    this.vlayer1 = new VectorLayer({
      source: new VectorSource({
        url: '../../assets/sr_webmercator.geojson',
        format: new GeoJSON()
      })
    });

    this.vlayer2 = new VectorLayer({
      source: new VectorSource({
        url: '../../assets/sr_wm.geojson',
        format: new GeoJSON()
      })
    });

    this.view = new OlView({
      center: fromLonLat([14.713160, 46.000000]),
      zoom: 8
    });

    this.map = new OlMap({
      target: 'map',
      layers: [this.tlayer, this.vlayer1],
      view: this.view
    });

  }
}