import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ConsoleComponent } from './console/console.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FileBrowserDirective } from './file-browser.directive';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    SidebarComponent,
    ConsoleComponent,
    ToolbarComponent,
    FileBrowserDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
