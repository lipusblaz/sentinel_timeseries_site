import sqlite3
import os
import sys


class SqliteConnector:

    def __init__(self, db, libdir=None):
        if libdir is None:
            if sys.platform == "linux" or sys.platform == "linux2":
                libdir = os.path.dirname(os.path.abspath(__file__)) + "/spatialite/linux"
            elif sys.platform == "darwin":
                libdir = os.path.dirname(os.path.abspath(__file__)) + "/spatialite/mac"
            elif sys.platform == "win32" or sys.platform == "win64":
                libdir = os.path.dirname(os.path.abspath(__file__)) + r"\spatialite\windows"

        os.environ["path"] = libdir + ';' + os.environ['path']

        conn = sqlite3.connect(db)
        conn.enable_load_extension(True)
        conn.execute("SELECT load_extension('mod_spatialite')")

        self.conn = conn

    def cursor(self):
        return self.conn.cursor()

    def execute(self, query):
        return self.conn.execute(query)

    def executemany(self, query, data):
        return self.conn.executemany(query, data)

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()
