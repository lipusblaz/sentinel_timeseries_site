import geojson
from spatialite_connector import *
import pprint

def get_geotable_fields(conn, table_name):
    q = f"PRAGMA table_info({table_name})"
    cur = conn.execute(q)
    fields = []
    for x in cur:
        if x[1] == "geometry":
            fields.insert(0, "AsGeoJSON(geometry)")
        elif x[1] == "GEOMETRY":
            fields.insert(0, "AsGeoJSON(GEOMETRY)")
        else:
            fields.append(x[1])
    return fields


def get_layer_all(conn, table_name):
    fields = get_geotable_fields(conn, table_name)
    qq = f"SELECT {', '.join(fields)} FROM {table_name}"
    res = [i for i in conn.execute(qq)]
    featureCollection = list()
    for row in res: # first input in row is always geometry. Other are in original order
        geom = geojson.loads(row[0])
        feature = geojson.Feature(geometry=geom, properties=row[1:])
        featureCollection.append(feature)

    return geojson.FeatureCollection(featureCollection)



if __name__=="__main__":
    db = "./data/slo_upravne_enote.sqlite"
    conn = SqliteConnector(db)

    print(get_geotable_fields(conn, "slo_upravne_enote"))
    aa = get_layer_all(conn, "slo_upravne_enote")
    print()




