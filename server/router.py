from flask import Flask
import os
import geojson
import json
from os import listdir

# my libraries
from spatialite_connector import *
from spatialite2geojson import *

app = Flask(__name__)

# environment variables
working_dir = os.path.dirname(os.path.abspath(__file__))

@app.route("/")
def inital_route():
    return "Dela nekaj"

@app.route("/GetLayers")
def getLayers():
    available = listdir("./data")
    return json.dumps(available)

@app.route("/GetLayer/<table>")
def getLayer(table):
    db_name = "./data/" + table + ".sqlite"
    conn = SqliteConnector(db_name)
    out_json = get_layer_all(conn, table)

    return geojson.dumps(out_json)


@app.route("/AddLayer/<localPath>")
def addLayer(localPath):
    all_db_files = [f for f in os.listdir("./data/") if f.split(".")[-1] == "sqlite"]
    out_json = []
    for f in all_db_files:
        with SqliteConnector(f) as sqlite:
            json = next(sqlite.execute)
    return str(all_db_files)

@app.route("/loadData/")
def load_data():
    pass

@app.route("/graph/<id_poly>")
def get_graph(id_poly):
    return "Graph %s" % (id_poly)


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8000, debug=True)
    #print(getLayer("slo_upravne_enote"))